

angular.module('myApp', [])
  .controller('DcController', function($scope, $http){
    $scope.$watch('search', function() {
      fetch();
    });

    $scope.search = "";

    function fetch(){
      
	  
	  
	 $http.get("https://phaidra.cab.unipd.it/api/object/o:" + $scope.search + "/dc")
     .then(function(response){ 
		$scope.details = response.data; 
		console.log(JSON.stringify($scope.details));
	});
	 
	 
	 
	 
	

     
    }

    $scope.update = function(dublincore){
      $scope.search = dublincore.metadata.dc;
    };

    $scope.select = function(){
      this.setSelectionRange(0, this.value.length);
    }
  });
