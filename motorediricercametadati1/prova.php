
<?php
$pid=$_POST['pid'];
$phaidradc = $_POST['phaidradc'];
$curl_handle=curl_init();
if ($curl_handle === null) {
	echo "Manca curl";
	die;
}
curl_setopt($curl_handle, CURLOPT_URL, 'https://phaidradev.cab.unipd.it/api/object/o:'.$phaidradc.'/dc');
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($curl_handle, CURLOPT_TIMEOUT, 20);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

//Required for http(s)
curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);


$buffer = curl_exec($curl_handle);
curl_close($curl_handle);
if (empty($buffer)){
  print "Nothing returned from url.<p>";
}
else{
  print $buffer;
 
}


require 'vendor/autoload.php';
$client = Elasticsearch\ClientBuilder::create()->build();
$params = array();
$params['body']  = array(
  'pid' => $pid,
  'dublincore' => $buffer
  
);

$params['index'] = 'phaidra';
$params['type']  = 'dc';

$result = $client->index($params);

?>